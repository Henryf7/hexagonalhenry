﻿using Hexagonal.Features.Login.Application.UseCases;
using HexagonalHenry.Features.Categorie.Application.Services;
using HexagonalHenry.Features.Categorie.Application.UseCases;
using HexagonalHenry.Features.Categorie.Domain.Ports.In;
using HexagonalHenry.Features.Categorie.Domain.Ports.Out;
using HexagonalHenry.Features.Categorie.Infrastructure;
using HexagonalHenry.Features.Login.Domain.Ports.In;
using HexagonalHenry.Features.Note.Application.Services;
using HexagonalHenry.Features.Note.Application.UseCases;
using HexagonalHenry.Features.Note.Domain.Ports.In;
using HexagonalHenry.Features.Note.Domain.Ports.Out;
using HexagonalHenry.Features.Note.Infrastructure;
using HexagonalHenry.Features.Tag.Application.Services;
using HexagonalHenry.Features.Tag.Application.UseCases;
using HexagonalHenry.Features.Tag.Domain.Ports.In;
using HexagonalHenry.Features.Tag.Domain.Ports.Out;
using HexagonalHenry.Features.Tag.Infrastructure;
using HexagonalHenry.Features.User.Application.Services;
using HexagonalHenry.Features.User.Application.UseCases;
using HexagonalHenry.Features.User.Domain.Ports.In;
using HexagonalHenry.Features.User.Domain.Ports.Out;
using HexagonalHenry.Features.User.Infrastructure;
using HexagonalHenry.Shared.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace HexagonalHenry
{
    public static class ServicesExtension
    {
        public static void AddSeederExtension(this IServiceCollection services)
        {
            ServiceProvider serviceProvider = services.BuildServiceProvider();
            using (AppDbContext? context = serviceProvider.GetService<AppDbContext>())
            {
                try
                {
                    if (context != null)
                    {
                        context.Database.Migrate(); // Asegurar que la base de datos está creada y migrada
                        //SeedData.Initialize(context); // Llamar al método de inicialización
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error al inicializar la base de datos: {ex.Message}");
                }
            }
        }

        public static void AddNoteExtension(this IServiceCollection services)
        {
            services.AddScoped<INoteDbAdapterPort, NoteDbAdapter>();
            services.AddScoped<INoteServicePort, NoteService>();
            services.AddScoped<IGetNotes, GetNotesImpl>();
            services.AddScoped<IGetNoteById, GetNoteByIdImpl>();
            services.AddScoped<ICreateNote, CreateNoteImpl>();
            services.AddScoped<IUpdateNote, UpdateNoteImpl>();
            services.AddScoped<IDeleteNote, DeleteNoteImpl>();
        }

        public static void AddUserExtension(this IServiceCollection services)
        {
            services.AddScoped<IUserDbAdapterPort, UserDbAdapter>();
            services.AddScoped<IGetUsers, GetUsersImpl>();
            services.AddScoped<IGetUserById, GetUserByIdImpl>();
            services.AddScoped<IGetUserByEmail, GetUserByEmailImpl>();
            services.AddScoped<ICreateUser, CreateUserImpl>();
            services.AddScoped<IUpdateUser, UpdateUserImpl>();
            services.AddScoped<IDeleteUser, DeleteUserImpl>();
            services.AddScoped<IUserServicePort, UserService>();
        }
        public static void AddLoginExtension(this IServiceCollection services)
        {
            services.AddScoped<ILogin, LoginImpl>();
        }

        public static void AddCategorieExtension(this IServiceCollection services)
        {
            services.AddScoped<ICategorieDbAdapterPort, CategorieDbAdapter>();
            services.AddScoped<IGetCategories, GetCategoriesImpl>();
            services.AddScoped<IGetCategorieById, GetCategorieByIdImpl>();
            services.AddScoped<ICreateCategorie, CreateCategorieImpl>();
            services.AddScoped<IUpdateCategorie, UpdateCategorieImpl>();
            services.AddScoped<IDeleteCategorie, DeleteCategorieImpl>();
            services.AddScoped<ICategorieServicePort, CategorieService>();
        }
        public static void AddTagExtension(this IServiceCollection services)
        {
            services.AddScoped<ITagDbAdapterPort, TagDbAdapter>();
            services.AddScoped<IGetTags, GetTagsImpl>();
            services.AddScoped<IGetTagById, GetTagByIdImpl>();
            services.AddScoped<ICreateTag, CreateTagImpl>();
            services.AddScoped<IUpdateTag, UpdateTagImpl>();
            services.AddScoped<IDeleteTag, DeleteTagImpl>();
            services.AddScoped<ITagServicePort, TagService>();
        }

    }
}
