﻿using HexagonalHenry.Features.Categorie.Domain.Models;
using HexagonalHenry.Features.Categorie.Domain.Ports.In;

namespace HexagonalHenry.Features.Categorie.Application.Services
{
    public class CategorieService : ICategorieServicePort
    {
        private readonly IGetCategories _getCategories;
        private readonly IGetCategorieById _getCategorieById;
        private readonly ICreateCategorie _createCategorie;
        private readonly IUpdateCategorie _updateCategorie;
        private readonly IDeleteCategorie _deleteCategorie;

        public CategorieService(IGetCategories getCategories, IGetCategorieById getCategorieById, ICreateCategorie createCategorie, IUpdateCategorie updateCategorie, IDeleteCategorie deleteCategorie)
        {
            _getCategories = getCategories;
            _getCategorieById = getCategorieById;
            _createCategorie = createCategorie;
            _updateCategorie = updateCategorie;
            _deleteCategorie = deleteCategorie;
        }

        public Task<List<CategorieModel>> GetCategories()
        {
            return _getCategories.GetCategories();
        }

        public Task<CategorieModel> GetCategorieById(int id)
        {
            return _getCategorieById.GetCategorieById(id);
        }

        public Task<CategorieModel> Create(CategorieModel categorie)
        {
            return _createCategorie.Create(categorie);
        }

        public Task<CategorieModel> Update(int id, CategorieModel categorie)
        {
            return _updateCategorie.Update(id, categorie);
        }

        public Task<int> Delete(int id)
        {
            return _deleteCategorie.Delete(id);
        }
    }
}
