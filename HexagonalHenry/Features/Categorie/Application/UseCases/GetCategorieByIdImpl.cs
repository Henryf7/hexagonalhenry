﻿using HexagonalHenry.Features.Categorie.Domain.Models;
using HexagonalHenry.Features.Categorie.Domain.Ports.In;
using HexagonalHenry.Features.Categorie.Domain.Ports.Out;

namespace HexagonalHenry.Features.Categorie.Application.UseCases
{
    public class GetCategorieByIdImpl : IGetCategorieById
    {
        private readonly ICategorieDbAdapterPort _adapter;

        public GetCategorieByIdImpl(ICategorieDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<CategorieModel> GetCategorieById(int id)
        {
            return _adapter.FindById(id);
        }
    }
}
