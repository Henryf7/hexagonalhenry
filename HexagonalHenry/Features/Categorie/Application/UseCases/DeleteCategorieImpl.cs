﻿using HexagonalHenry.Features.Categorie.Domain.Ports.In;
using HexagonalHenry.Features.Categorie.Domain.Ports.Out;

namespace HexagonalHenry.Features.Categorie.Application.UseCases
{
    public class DeleteCategorieImpl : IDeleteCategorie
    {
        private readonly ICategorieDbAdapterPort _adapter;

        public DeleteCategorieImpl(ICategorieDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<int> Delete(int id)
        {
            return _adapter.Delete(id);
        }
    }
}
