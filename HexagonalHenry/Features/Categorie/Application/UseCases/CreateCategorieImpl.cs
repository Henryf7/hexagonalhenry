﻿using HexagonalHenry.Features.Categorie.Domain.Models;
using HexagonalHenry.Features.Categorie.Domain.Ports.In;
using HexagonalHenry.Features.Categorie.Domain.Ports.Out;

namespace HexagonalHenry.Features.Categorie.Application.UseCases
{
    public class CreateCategorieImpl : ICreateCategorie
    {
        private readonly ICategorieDbAdapterPort _adapter;

        public CreateCategorieImpl(ICategorieDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<CategorieModel> Create(CategorieModel categorie)
        {
            return _adapter.Create(categorie);
        }
    }
}
