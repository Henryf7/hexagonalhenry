﻿using HexagonalHenry.Features.Categorie.Domain.Models;
using HexagonalHenry.Features.Categorie.Domain.Ports.In;
using HexagonalHenry.Features.Categorie.Domain.Ports.Out;

namespace HexagonalHenry.Features.Categorie.Application.UseCases
{
    public class GetCategoriesImpl : IGetCategories
    {
        private readonly ICategorieDbAdapterPort _adapter;

        public GetCategoriesImpl(ICategorieDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<List<CategorieModel>> GetCategories()
        {
            return _adapter.FindAll();
        }
    }
}
