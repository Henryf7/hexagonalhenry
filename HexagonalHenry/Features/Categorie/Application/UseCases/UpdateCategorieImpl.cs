﻿using HexagonalHenry.Features.Categorie.Domain.Models;
using HexagonalHenry.Features.Categorie.Domain.Ports.In;
using HexagonalHenry.Features.Categorie.Domain.Ports.Out;

namespace HexagonalHenry.Features.Categorie.Application.UseCases
{
    public class UpdateCategorieImpl : IUpdateCategorie
    {
        private readonly ICategorieDbAdapterPort _adapter;

        public UpdateCategorieImpl(ICategorieDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<CategorieModel> Update(int id, CategorieModel categorie)
        {
            return _adapter.Update(id, categorie);
        }
    }
}
