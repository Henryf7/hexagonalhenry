﻿using HexagonalHenry.Features.User.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace HexagonalHenry.Features.Categorie.Domain.Models
{
    public class CategorieModel
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }
    }
}
