﻿using HexagonalHenry.Features.Categorie.Domain.Models;
using HexagonalHenry.Shared.Domain.Ports.Out;

namespace HexagonalHenry.Features.Categorie.Domain.Ports.Out
{
    public interface ICategorieDbAdapterPort : IDbCrudAdapter<CategorieModel>
    {
    }
}
