﻿using HexagonalHenry.Features.Categorie.Domain.Models;

namespace HexagonalHenry.Features.Categorie.Domain.Ports.In
{
    public interface ICreateCategorie
    {
        Task<CategorieModel> Create(CategorieModel tag);
    }
}
