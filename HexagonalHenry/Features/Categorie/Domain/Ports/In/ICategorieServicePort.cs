﻿namespace HexagonalHenry.Features.Categorie.Domain.Ports.In
{
    public interface ICategorieServicePort : IGetCategories, IGetCategorieById, ICreateCategorie, IUpdateCategorie, IDeleteCategorie
    {
    }
}
