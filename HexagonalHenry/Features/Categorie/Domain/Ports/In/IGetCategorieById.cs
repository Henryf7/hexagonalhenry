﻿using HexagonalHenry.Features.Categorie.Domain.Models;

namespace HexagonalHenry.Features.Categorie.Domain.Ports.In
{
    public interface IGetCategorieById
    {
        Task<CategorieModel> GetCategorieById(int id);
    }
}
