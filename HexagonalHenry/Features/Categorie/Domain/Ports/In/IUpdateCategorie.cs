﻿using HexagonalHenry.Features.Categorie.Domain.Models;

namespace HexagonalHenry.Features.Categorie.Domain.Ports.In
{
    public interface IUpdateCategorie
    {
        Task<CategorieModel> Update(int id, CategorieModel categorie);
    }
}
