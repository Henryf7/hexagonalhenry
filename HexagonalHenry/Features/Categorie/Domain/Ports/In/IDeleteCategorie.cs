﻿namespace HexagonalHenry.Features.Categorie.Domain.Ports.In
{
    public interface IDeleteCategorie
    {
        Task<int> Delete(int id);
    }
}
