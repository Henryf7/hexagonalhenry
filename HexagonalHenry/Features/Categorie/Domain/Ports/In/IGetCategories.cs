﻿using HexagonalHenry.Features.Categorie.Domain.Models;

namespace HexagonalHenry.Features.Categorie.Domain.Ports.In
{
    public interface IGetCategories
    {
        Task<List<CategorieModel>> GetCategories();
    }
}
