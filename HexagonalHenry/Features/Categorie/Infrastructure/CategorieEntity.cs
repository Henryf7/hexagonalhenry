﻿using HexagonalHenry.Features.Categorie.Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HexagonalHenry.Features.Categorie.Infrastructure
{
    [Table("Categorie")]
    public class CategorieEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        public CategorieEntity FromModel(CategorieModel model)
        {
            Id = model.Id;
            Name = model.Name;
            return this;
        }

        public CategorieModel ToModel()
        {
            return new CategorieModel
            {
                Id = Id,
                Name = Name,
            };
        }

        public void UpdatePropsFromModel(CategorieModel model)
        {
            Name = model.Name ?? Name;
        }
    }
}
