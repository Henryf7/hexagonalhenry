﻿using HexagonalHenry.Features.Categorie.Domain.Models;
using HexagonalHenry.Features.Categorie.Domain.Ports.Out;
using HexagonalHenry.Shared.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace HexagonalHenry.Features.Categorie.Infrastructure
{
    public class CategorieDbAdapter : ICategorieDbAdapterPort
    {
        private readonly AppDbContext _db;

        public CategorieDbAdapter(AppDbContext context)
        {
            _db = context;
        }

        public async Task<List<CategorieModel>> FindAll()
        {
            List<CategorieModel> categories = await _db.Categories
                .Select(x => x.ToModel())
                .ToListAsync();

            return categories;
        }

        public async Task<CategorieModel> FindById(int id)
        {
            CategorieEntity? categorieFound = await _db.Categories
                .FirstOrDefaultAsync(i => i.Id == id);

            return categorieFound == null
                ? throw new Exception($"Categorie with id {id} not found")
                : categorieFound.ToModel();
        }

        public async Task<CategorieModel> Create(CategorieModel categorie)
        {
            try
            {
                CategorieEntity entity = new CategorieEntity().FromModel(categorie);
                _db.Categories.Add(entity);
                await _db.SaveChangesAsync();
                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error creating Categorie", e);
            }
        }

        public async Task<CategorieModel> Update(int id, CategorieModel categorie)
        {
            CategorieEntity? entity = await _db.Categories.FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
            {
                throw new Exception("Categorie not found!");
            }

            try
            {
                entity.UpdatePropsFromModel(categorie);

                _db.Entry(entity).State = EntityState.Modified;
                await _db.SaveChangesAsync();

                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error updating Categorie", e);
            }
        }

        public async Task<int> Delete(int id)
        {
            CategorieEntity? categorieEntity = await _db.Categories.FindAsync(id);

            if (categorieEntity == null)
            {
                throw new Exception("Categorie not found!");
            }

            try
            {
                _db.Categories.Remove(categorieEntity);

                await _db.SaveChangesAsync();

                return id;
            }
            catch (Exception e)
            {
                throw new Exception("Error deleting Categorie", e);
            }
        }
    }
}
