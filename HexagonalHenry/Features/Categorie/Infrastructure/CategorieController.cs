﻿using HexagonalHenry.Features.Categorie.Domain.Models;
using HexagonalHenry.Features.Categorie.Domain.Ports.In;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HexagonalHenry.Features.Categorie.Infrastructure
{
    [Route("api/Categories")]
    [ApiController]
    public class CategorieController : ControllerBase
    {
        private readonly ICategorieServicePort _service;

        public CategorieController(ICategorieServicePort service)
        {
            _service = service;
        }

        // GET: api/<CategorieController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategorieModel>>> Get()
        {
            return await _service.GetCategories();
        }

        // GET api/<CategorieController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CategorieModel>> Get(int id)
        {
            CategorieModel categorieModel = await _service.GetCategorieById(id);
            if (categorieModel == null)
            {
                return NotFound();
            }
            return categorieModel;
        }

        // POST api/<CategorieController>
        [HttpPost]
        public async Task<ActionResult<CategorieModel>> Post([FromBody] CategorieModel categorieModel)
        {
            try
            {
                CategorieModel modelSaved = await _service.Create(categorieModel);
                return CreatedAtAction("Get", new { id = modelSaved.Id }, modelSaved);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PATCH api/<CategorieController>/5
        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody] CategorieModel categorieModel)
        {
            try
            {
                await _service.Update(id, categorieModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }

        // DELETE api/<CategorieController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }
    }
}
