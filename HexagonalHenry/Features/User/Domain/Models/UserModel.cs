﻿using HexagonalHenry.Features.Note.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace HexagonalHenry.Features.User.Domain.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string? LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
