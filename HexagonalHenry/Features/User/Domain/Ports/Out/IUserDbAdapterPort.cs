﻿using HexagonalHenry.Features.User.Domain.Models;
using HexagonalHenry.Shared.Domain.Ports.Out;

namespace HexagonalHenry.Features.User.Domain.Ports.Out
{
    public interface IUserDbAdapterPort : IDbCrudAdapter<UserModel>
    {
        Task<UserModel?> FindByEmail(string email);
    }
}
