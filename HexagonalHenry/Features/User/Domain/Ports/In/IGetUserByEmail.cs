﻿using HexagonalHenry.Features.User.Domain.Models;

namespace HexagonalHenry.Features.User.Domain.Ports.In
{
    public interface IGetUserByEmail
    {
        Task<UserModel?> GetUserByEmail(string email);
    }
}
