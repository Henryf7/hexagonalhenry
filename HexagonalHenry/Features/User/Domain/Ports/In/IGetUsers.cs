﻿using HexagonalHenry.Features.User.Domain.Models;

namespace HexagonalHenry.Features.User.Domain.Ports.In
{
    public interface IGetUsers
    {
        Task<List<UserModel>> GetUsers();
    }
}
