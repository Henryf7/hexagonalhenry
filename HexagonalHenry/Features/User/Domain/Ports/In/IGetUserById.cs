﻿using HexagonalHenry.Features.User.Domain.Models;

namespace HexagonalHenry.Features.User.Domain.Ports.In
{
    public interface IGetUserById
    {
        Task<UserModel?> GetUserById(int id);
    }
}
