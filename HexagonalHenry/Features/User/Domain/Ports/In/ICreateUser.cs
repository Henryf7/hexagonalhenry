﻿using HexagonalHenry.Features.User.Domain.Models;

namespace HexagonalHenry.Features.User.Domain.Ports.In
{
    public interface ICreateUser
    {
        Task<UserModel> Create(UserModel user);
    }
}
