﻿using HexagonalHenry.Features.User.Domain.Models;
using HexagonalHenry.Features.User.Domain.Ports.In;

namespace HexagonalHenry.Features.User.Application.Services
{
    public class UserService : IUserServicePort
    {
        private readonly IGetUsers _getUSersService;
        private readonly IGetUserById _getUSerByIdService;
        private readonly ICreateUser _createUserService;
        private readonly IUpdateUser _updateUserService;
        private readonly IDeleteUser _deleteUserService;

        public UserService(
            IGetUsers getUsersService,
            IGetUserById getUserByIdService,
            ICreateUser createUserService,
            IUpdateUser updateUserService,
            IDeleteUser deleteUserService
            )
        {
            _getUSersService = getUsersService;
            _getUSerByIdService = getUserByIdService;
            _createUserService = createUserService;
            _updateUserService = updateUserService;
            _deleteUserService = deleteUserService;
        }

        public Task<List<UserModel>> GetUsers()
        {
            return _getUSersService.GetUsers();
        }

        public Task<UserModel?> GetUserById(int id)
        {
            return _getUSerByIdService.GetUserById(id);
        }

        public Task<UserModel> Create(UserModel user)
        {
            return _createUserService.Create(user);
        }

        public Task<UserModel> Update(int id, UserModel user)
        {
            return _updateUserService.Update(id, user);
        }

        public Task<int> Delete(int id)
        {
            return _deleteUserService.Delete(id);
        }
    }
}
