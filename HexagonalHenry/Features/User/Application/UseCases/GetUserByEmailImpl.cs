﻿using HexagonalHenry.Features.User.Domain.Models;
using HexagonalHenry.Features.User.Domain.Ports.In;
using HexagonalHenry.Features.User.Domain.Ports.Out;

namespace HexagonalHenry.Features.User.Application.UseCases
{
    public class GetUserByEmailImpl : IGetUserByEmail
    {
        private readonly IUserDbAdapterPort _adapter;

        public GetUserByEmailImpl(IUserDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<UserModel?> GetUserByEmail(string email)
        {
            return _adapter.FindByEmail(email);
        }
    }
}
