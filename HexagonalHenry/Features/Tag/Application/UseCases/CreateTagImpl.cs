﻿using HexagonalHenry.Features.Tag.Domain.Models;
using HexagonalHenry.Features.Tag.Domain.Ports.In;
using HexagonalHenry.Features.Tag.Domain.Ports.Out;

namespace HexagonalHenry.Features.Tag.Application.UseCases
{
    public class CreateTagImpl : ICreateTag
    {
        private readonly ITagDbAdapterPort _adapter;

        public CreateTagImpl(ITagDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<TagModel> Create(TagModel tag)
        {
            return _adapter.Create(tag);
        }
    }
}
