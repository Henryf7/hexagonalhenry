﻿using HexagonalHenry.Features.Tag.Domain.Models;
using HexagonalHenry.Features.Tag.Domain.Ports.In;
using HexagonalHenry.Features.Tag.Domain.Ports.Out;

namespace HexagonalHenry.Features.Tag.Application.UseCases
{
    public class UpdateTagImpl : IUpdateTag
    {
        private readonly ITagDbAdapterPort _adapter;

        public UpdateTagImpl(ITagDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<TagModel> Update(int id, TagModel tag)
        {
            return _adapter.Update(id, tag);
        }
    }
}
