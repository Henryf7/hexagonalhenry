﻿using HexagonalHenry.Features.Tag.Domain.Models;
using HexagonalHenry.Shared.Domain.Ports.Out;

namespace HexagonalHenry.Features.Tag.Domain.Ports.Out
{
    public interface ITagDbAdapterPort : IDbCrudAdapter<TagModel>
    {
    }
}
