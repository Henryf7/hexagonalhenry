﻿using HexagonalHenry.Features.Tag.Domain.Models;

namespace HexagonalHenry.Features.Tag.Domain.Ports.In
{
    public interface IUpdateTag
    {
        Task<TagModel> Update(int id, TagModel tag);
    }
}
