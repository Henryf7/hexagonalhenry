﻿using HexagonalHenry.Features.Tag.Domain.Models;

namespace HexagonalHenry.Features.Tag.Domain.Ports.In
{
    public interface IGetTagById
    {
        Task<TagModel> GetTagById(int id);
    }
}
