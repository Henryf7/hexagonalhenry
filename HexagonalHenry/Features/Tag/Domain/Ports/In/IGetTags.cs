﻿using HexagonalHenry.Features.Tag.Domain.Models;

namespace HexagonalHenry.Features.Tag.Domain.Ports.In
{
    public interface IGetTags
    {
        Task<List<TagModel>> GetTags();
    }
}
