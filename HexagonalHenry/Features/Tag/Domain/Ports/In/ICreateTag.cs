﻿using HexagonalHenry.Features.Tag.Domain.Models;

namespace HexagonalHenry.Features.Tag.Domain.Ports.In
{
    public interface ICreateTag
    {
        Task<TagModel> Create(TagModel tag);
    }
}
