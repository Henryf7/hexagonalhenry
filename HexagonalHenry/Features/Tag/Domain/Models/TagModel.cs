﻿using HexagonalHenry.Features.Note.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace HexagonalHenry.Features.Tag.Domain.Models
{
    public class TagModel
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }
        public virtual ICollection<NoteModel> Notes { get; set; }
    }
}
