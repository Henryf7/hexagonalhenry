﻿using HexagonalHenry.Features.Note.Infrastructure;
using HexagonalHenry.Features.Tag.Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HexagonalHenry.Features.Tag.Infrastructure
{
    [Table("Tag")]
    public class TagEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public virtual ICollection<NotesEntity> Notes { get; set; }

        public TagEntity FromModel(TagModel model)
        {
            Id = model.Id;
            Name = model.Name;
            return this;
        }

        public TagModel ToModel()
        {
            return new TagModel
            {
                Id = Id,
                Name = Name,
            };
        }

        public void UpdatePropsFromModel(TagModel model)
        {
            Name = model.Name ?? Name;
        }
    }
}
