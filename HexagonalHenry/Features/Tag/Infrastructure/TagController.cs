﻿using HexagonalHenry.Features.Tag.Domain.Ports.In;
using HexagonalHenry.Features.Tag.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace HexagonalHenry.Features.Tag.Infrastructure
{
    [Route("api/Tags")]
    [ApiController]
    public class TagController : ControllerBase
    {
        private readonly ITagServicePort _service;

        public TagController(ITagServicePort service)
        {
            _service = service;
        }

        // GET: api/<TagController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TagModel>>> Get()
        {
            return await _service.GetTags();
        }

        // GET api/<TagController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TagModel>> Get(int id)
        {
            TagModel tagModel = await _service.GetTagById(id);
            if (tagModel == null)
            {
                return NotFound();
            }
            return tagModel;
        }

        // POST api/<TagController>
        [HttpPost]
        public async Task<ActionResult<TagModel>> Post([FromBody] TagModel tagModel)
        {
            try
            {
                TagModel modelSaved = await _service.Create(tagModel);
                return CreatedAtAction("Get", new { id = modelSaved.Id }, modelSaved);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PATCH api/<TagController>/5
        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody] TagModel tagModel)
        {
            try
            {
                await _service.Update(id, tagModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }

        // DELETE api/<TagController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return NoContent();
        }
    }
}
