﻿using HexagonalHenry.Features.Login.Domain.Models;

namespace HexagonalHenry.Features.Login.Domain.Ports.In
{
    public interface ILogin
    {
        Task<string> Login(LoginModel login);
    }
}
