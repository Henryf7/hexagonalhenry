﻿namespace HexagonalHenry.Features.Login.Domain
{
    public class CredentialsException : Exception
    {
        public CredentialsException() : base("Invalid credentials")
        {
        }
    }
}
