﻿using HexagonalHenry.Features.Note.Domain.Models;
using HexagonalHenry.Features.Note.Domain.Ports.In;
using HexagonalHenry.Features.Note.Domain.Ports.Out;

namespace HexagonalHenry.Features.Note.Application.UseCases
{
    public class CreateNoteImpl: ICreateNote
    {
        private readonly INoteDbAdapterPort _adapter;

        public CreateNoteImpl(INoteDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<NoteModel> Create(NoteModel note)
        {
            return _adapter.Create(note);
        }

    }
}
