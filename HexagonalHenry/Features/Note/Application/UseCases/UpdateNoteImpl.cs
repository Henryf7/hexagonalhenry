﻿using HexagonalHenry.Features.Note.Domain.Models;
using HexagonalHenry.Features.Note.Domain.Ports.In;
using HexagonalHenry.Features.Note.Domain.Ports.Out;

namespace HexagonalHenry.Features.Note.Application.UseCases
{
    public class UpdateNoteImpl: IUpdateNote
    {
        private readonly INoteDbAdapterPort _adapter;

        public UpdateNoteImpl(INoteDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<NoteModel> Update(int id, NoteModel note)
        {
            return _adapter.Update(id, note);
        }

    }
}
