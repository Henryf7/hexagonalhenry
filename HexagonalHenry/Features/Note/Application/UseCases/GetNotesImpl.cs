﻿using HexagonalHenry.Features.Note.Domain.Models;
using HexagonalHenry.Features.Note.Domain.Ports.In;
using HexagonalHenry.Features.Note.Domain.Ports.Out;

namespace HexagonalHenry.Features.Note.Application.UseCases
{
    public class GetNotesImpl : IGetNotes
    {
        private readonly INoteDbAdapterPort _adapter;

        public GetNotesImpl(INoteDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<List<NoteModel>> GetNotes()
        {
            return _adapter.FindAll();
        }

    }
}
