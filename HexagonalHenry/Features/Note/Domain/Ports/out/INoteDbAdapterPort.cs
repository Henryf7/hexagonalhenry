﻿using HexagonalHenry.Features.Note.Domain.Models;
using HexagonalHenry.Shared.Domain.Ports.Out;

namespace HexagonalHenry.Features.Note.Domain.Ports.Out
{
    public interface INoteDbAdapterPort : IDbCrudAdapter<NoteModel>
    {
    }
}