﻿using HexagonalHenry.Features.Note.Domain.Models;

namespace HexagonalHenry.Features.Note.Domain.Ports.In
{
    public interface IGetNotes
    {
        Task<List<NoteModel>> GetNotes();
    }
}
