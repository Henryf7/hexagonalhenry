﻿using HexagonalHenry.Features.Note.Domain.Models;

namespace HexagonalHenry.Features.Note.Domain.Ports.In
{
    public interface IUpdateNote
    {
        Task<NoteModel> Update(int id, NoteModel note);
    }
}
