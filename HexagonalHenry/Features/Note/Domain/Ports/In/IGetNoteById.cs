﻿using HexagonalHenry.Features.Note.Domain.Models;

namespace HexagonalHenry.Features.Note.Domain.Ports.In
{
    public interface IGetNoteById
    {
        Task<NoteModel> GetNoteById(int id);
    }
}
