﻿using HexagonalHenry.Features.Note.Domain.Models;

namespace HexagonalHenry.Features.Note.Domain.Ports.In
{
    public interface ICreateNote
    {
        Task<NoteModel> Create(NoteModel note);
    }
}
