﻿using HexagonalHenry.Features.Categorie.Domain.Models;
using HexagonalHenry.Features.Tag.Domain.Models;
using HexagonalHenry.Features.User.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace HexagonalHenry.Features.Note.Domain.Models
{
    public class NoteModel
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Title { get; set; } = null!;
        [StringLength(100)]
        public string? Content { get; set; } = null!;
        public DateTime? CreationDate { get; set; }
        public int UserId { get; set; }
        public virtual UserModel User { get; set; }
        public int CategorieId { get; set; }
        public virtual CategorieModel Categorie { get; set; }
        public virtual ICollection<TagModel> Tags { get; set; }
    }
}
