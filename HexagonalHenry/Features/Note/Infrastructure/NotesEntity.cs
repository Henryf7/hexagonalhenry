﻿using HexagonalHenry.Features.Categorie.Infrastructure;
using HexagonalHenry.Features.Note.Domain.Models;
using HexagonalHenry.Features.Tag.Infrastructure;
using HexagonalHenry.Features.User.Infrastructure;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace HexagonalHenry.Features.Note.Infrastructure
{
    [Table("Note")]
    public class NotesEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Title { get; set; } = null!;
        [StringLength(100)]
        public string? Content { get; set; } = null!;
        public DateTime CreationDate { get; set; } = DateTime.Now;

        [ForeignKey("UserId")]
        public int UserId { get; set; }

        public virtual UserEntity User { get; set; }

        [ForeignKey("CategorieId")]
        public int CategorieId { get; set; }

        public virtual CategorieEntity Categorie { get; set; }
        public virtual ICollection<TagEntity> Tags { get; set; }

        public NotesEntity FromModel(NoteModel model)
        {
            Title = model.Title;
            Content = model.Content;
            UserId = model.UserId;
            CategorieId = model.CategorieId;
            CreationDate = model.CreationDate ?? DateTime.Now;
            return this;
        }

        public NoteModel ToModel()
        {
            return new NoteModel
            {
                Id = Id,
                Title = Title,
                Content = Content,
                UserId = UserId,
                CategorieId = CategorieId,
                CreationDate = CreationDate,
            };
        }

        public void UpdatePropsFromModel(NoteModel model)
        {
            Title = model.Title ?? Title;
            Content = model.Content ?? Content;
            CreationDate = model.CreationDate ?? CreationDate;

            if (!model.UserId.Equals(default))
                UserId = model.UserId;
            if (!model.CategorieId.Equals(default))
                CategorieId = model.CategorieId;
        }
    }
}
