﻿using HexagonalHenry.Features.Note.Domain.Models;
using HexagonalHenry.Features.Note.Domain.Ports.Out;
using HexagonalHenry.Shared.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace HexagonalHenry.Features.Note.Infrastructure
{
    public class NoteDbAdapter : INoteDbAdapterPort
    {
        private readonly AppDbContext _db;
        //private readonly DapperContext _dapperContext;

        public NoteDbAdapter(AppDbContext db /*DapperContext dapperContext*/)
        {
            _db = db;
            //_dapperContext = dapperContext;
        }

        public async Task<List<NoteModel>> FindAll()
        {
            List<NoteModel> courses = await _db.Notes
                .Select(x => x.ToModel())
                .ToListAsync();

            return courses;
        }

        public async Task<NoteModel> FindById(int id)
        {
            NotesEntity? courseFound = await _db.Notes.FirstOrDefaultAsync(i => i.Id == id);

            return courseFound == null
                ? throw new Exception($"Note with id {id} not found")
                : courseFound.ToModel();
        }

        public async Task<NoteModel> Create(NoteModel model)
        {
            try
            {
                NotesEntity entity = new NotesEntity().FromModel(model);
                _db.Notes.Add(entity);
                await _db.SaveChangesAsync();
                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error creating note", e);
            }
        }

        public async Task<NoteModel> Update(int id, NoteModel model)
        {
            NotesEntity? entity = await _db.Notes.FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
            {
                throw new Exception("Note not found!");
            }

            try
            {
                entity.UpdatePropsFromModel(model);

                _db.Entry(entity).State = EntityState.Modified;
                await _db.SaveChangesAsync();

                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error updating note", e);
            }
        }

        public async Task<int> Delete(int id)
        {
            NotesEntity? courseEntity = await _db.Notes.FindAsync(id);

            if (courseEntity == null)
            {
                throw new Exception("Note not found!");
            }

            try
            {
                _db.Notes.Remove(courseEntity);

                await _db.SaveChangesAsync();

                return id;
            }
            catch (Exception e)
            {
                throw new Exception("Error deleting note", e);
            }
        }

    }
}
