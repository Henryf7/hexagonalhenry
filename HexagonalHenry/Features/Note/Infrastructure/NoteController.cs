﻿using HexagonalHenry.Features.Note.Domain.Models;
using HexagonalHenry.Features.Note.Domain.Ports.In;
using HexagonalHenry.Features.User.Domain.Ports.In;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HexagonalHenry.Features.Note.Infrastructure
{
    [Route("api/Notes")]
    [ApiController]
    [Authorize]
    public class NoteController : ControllerBase
    {
        private readonly INoteServicePort _service;

        public NoteController(INoteServicePort service, IGetUserById getUserByIdService)
        {
            _service = service;
        }

        // GET: api/<NoteController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<NoteModel>>> Get()
        {
            return await _service.GetNotes();
        }

        // GET api/<NoteController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<NoteModel>> Get(int id)
        {
            NoteModel courseModel = await _service.GetNoteById(id);
            if (courseModel == null)
            {
                return NotFound();
            }
            return courseModel;
        }

        // POST api/<NoteController>
        [HttpPost]
        public async Task<ActionResult<NoteModel>> Post([FromBody] NoteModel noteModel)
        {
            try
            {
                NoteModel modelSaved = await _service.Create(noteModel);
                return CreatedAtAction("Get", new { id = modelSaved.Id }, modelSaved);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PATCH api/<NoteController>/5
        [HttpPatch("{id}")]
        public async Task<IActionResult> Patch(int id, [FromBody] NoteModel noteModel)
        {
            try
            {
                await _service.Update(id, noteModel);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }

        // DELETE api/<NoteController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _service.Delete(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }
    }

}
