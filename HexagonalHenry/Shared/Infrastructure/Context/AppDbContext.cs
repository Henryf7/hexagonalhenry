﻿using HexagonalHenry.Features.Categorie.Infrastructure;
using HexagonalHenry.Features.Note.Infrastructure;
using HexagonalHenry.Features.Tag.Infrastructure;
using HexagonalHenry.Features.User.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace HexagonalHenry.Shared.Infrastructure.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<NotesEntity> Notes { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<CategorieEntity> Categories { get; set; }
        public DbSet<TagEntity> Tags { get; set; }
    }
}

